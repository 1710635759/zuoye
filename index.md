## 第一天
# 简述一下对promise的理解
Promise的两个特点
1、Promise对象的状态不受外界影响
1）pending 初始状态
2）fulfilled 成功状态
3）rejected 失败状态
Promise 有以上三种状态，只有异步操作的结果可以决定当前是哪一种状态，其他任何操作都无法改变这个状态
2、Promise的状态一旦改变，就不会再变，任何时候都可以得到这个结果，状态不可以逆，只能由 pending变成fulfilled或者由pending变成rejected
Promise的三个缺点
1）无法取消Promise,一旦新建它就会立即执行，无法中途取消
2）如果不设置回调函数，Promise内部抛出的错误，不会反映到外部
3）当处于pending状态时，无法得知目前进展到哪一个阶段，是刚刚开始还是即将完成

# 手写实现promise
const PENDING = 'pending';
const FULFILLED = 'fulfilled';
const REJECTED = 'rejected;
//开始书写 构造函数
function myPromise(fn) {
	const that = this;  //保存的是myPromise这个构造函数的this，防止闭包（口区.gif）
  	that.value = null;
  	that.status = PENDING;
  	function resolve(value) { //这里的value是我们传进去的参数，可以保存在Promise内部
  		if(that.status === PENDING) {
  			that.value = value;
  			that.status = FULFILLED;
  		}
  	}
  	function reject(value) { //这里的value是我们传进去的参数，可以保存在Promise内部
  		if(that.status === PENDING) {
  			that.value = value;
  			that.status = REJECTED;
  		}
  	}
	try {
		fn(resolve,reject);
	}catch(e) {
		reject(e)
	}
	myPromise.prototype.then = function(onFulfilled,onRejected) { // 这里两个参数就是 成功/失败 对应的回调函数，取名字不同而已，莫慌 
		let self = this; //self是myPromise这个构造函数的this
		if(self.status === FULFILLED) {
			onFulfilled(self.value)  // 可以调用参数，默认是之前状态改变时保存的数据
		}
		if(self.status === REJECTED) {
			onRejected(self.value)
		}
	}
}

简述一下promise.all的使用方法，补全以下代码：
# Promise.all() 方法返回一个 Promise 实例
Promise.all() 方法返回一个 Promise 实例，此实例在()参数内所有的 promise 都“完成”或参数中不包含 promise 时回调完成；如果参数中 promise 有一个失败，此实例回调失败（reject），失败原因的是第一个失败 promise 的结果。

Promise.myAll = function(promises) {
}
const promise1 = new Promise((_, reject) => {
  setTimeout(() => {
    reject(new Error('报错了'))
  },2000)
})
const promise2 = new Promise(resolve => {
  setTimeout(() => {
    resolve('1')
  }, 1000)
})

const promise3 = new Promise(resolve => {
  setTimeout(() => {
    resolve('2')
  }, 999)
})

// 这个应该输出 ['1','2','3'] 顺序是固定的
Promise.myAll([promise2,promise3,'3']).then(res => {
console.log(res)
})

// 这个会进入到 catch
Promise.myAll([promise1, promise2,promise3,'3']).then(res => {
console.log(res)
}).catch(error => {
console.log('报错了')
})

# 简述一下promise.race的使用方法, 补全以下代码：
Promise.race(iterable) 方法返回一个 promise，一旦迭代器中的某个promise解决或拒绝，返回的 promise就会解决或拒绝。

Promise.myRace = function(promises) {
}
const promise1 = new Promise((_, reject) => {
  setTimeout(() => {
    reject(new Error('报错了'))
  },2000)
})
const promise2 = new Promise(resolve => {
  setTimeout(() => {
    resolve('1')
  }, 1000)
})

const promise3 = new Promise(resolve => {
  setTimeout(() => {
    resolve('2')
  }, 999)
})

Promise.myRace([promise1, promise2, promise3]).then(res => {
// 输出2
console.log(res)
})

Promise.myRace([promise1,'3', promise2, promise3]).then(res => {
// 输出3
console.log(res)
})


## 第二天
# 场景题： 进入页面初始同时请求10个接口，如果有一个接口失败了，其他9个接口成功。我能通过什么方法拿到所有接口的请求状态。
# 如何实现一个图片懒加载
let imgList = [...document.querySelectorAll('img')]
let length = imgList.length

const imgLazyLoad = function() {
    let count = 0
    return (function() {
        let deleteIndexList = []
        imgList.forEach((img, index) => {
            let rect = img.getBoundingClientRect()
            if (rect.top < window.innerHeight) {
                img.src = img.dataset.src
                deleteIndexList.push(index)
                count++
                if (count === length) {
                    document.removeEventListener('scroll', imgLazyLoad)
                }
            }
        })
        imgList = imgList.filter((img, index) => !deleteIndexList.includes(index))
    })()
}
document.addEventListener('scroll', imgLazyLoad)

# 如何实现message方法，实现api形式的组件调用

## 第三天
# 实现 new 关键字
function person(name, age) {
  this.name = name
  this.age = age
  return this
}
function objectFactory() {
	const obj = new Object(); // 1. 创建一个新的对象
	const Constructor = [].shift.call(arguments);  
	obj.__proto__ = Constructor.prototype; // 2. 把Person方法的原型挂载到了obj的原型链上
	const ret = Constructor.apply(obj, arguments); 
	return typeof ret === "object" ? ret : obj;  // 3. 返回一个新的对象
}
let p = objectFactory(person, '布兰', 12)console.log(p)  // { name: '布兰', age: 12 }
# 简述对bind方法的理解，补全以下代码：
JS中把基于闭包预先处理事情的思想叫做：柯理化函数思想（体现了闭包的保存作用）
Function.prototype.myBind = function myBind(context) {
    //=>this:fn也就是我们需要处理的函数
    var _this = this,
        outerAry = [].slice.call(arguments, 1);//=>获取除了第一项CONTEXT之外其余传递进来的值
    return function () {
        //=>里层函数在调用的时候,可以会被传递一些值,例如:事件对象
        var innerAry = [].slice.call(arguments, 0);
        _this.apply(context, outerAry.concat(innerAry));
    }
};
// setTimeout(fn.myBind(obj, 10, 20), 1000);
document.body.onclick = fn.myBind(obj, 10, 20);
# 简述apply和call方法的区别，总结以下this指向的结果，以及为什么产生该结果的原因？
apply：最多只能有两个参数——新this对象和一个数组 Array。如果给该方法传递多个参数，则把参数都写进这个数组里面，当然，即使只有一个参数，也要写进数组里面。如果 Array 不是一个有效的数组或者不是 arguments 对象，那么将导致一个 TypeError。如果没有提供 Array 和 Obj 任何一个参数，那么 Global 对象将被用作 Obj， 并且无法被传递任何参数。
call：则是直接的参数列表，主要用在js对象各方法互相调用的时候，使当前this实例指针保持一致,或在特殊情况下需要改变this指针。如果没有提供 Obj 参数，那么 Global 对象被用作 Obj。
# 实现函数防抖功能
var btn = document.getElementById('btn');
btn.addEventListener('click',fn(()=>{
    console.log('12321');
}));

function fn(callback){
    let timer = null;
    return function(){
        console.log('click')
        clearTimeout(timer);
        timer = setTimeout(()=>{
            callback()
        },3000);
    }
}

# 实现函数节流功能
const throttle = (callback) => {
let flag = true
	return () => {
		console.log('click')
		if(!flag) return
		flag = false;
		setTimeout(() => {
			  flag = true;
			  callback()
		}, 3000)	
	}
}

let btn = document.querySelector('#btn');
btn.addEventListener('click', throttle(() => {
	console.log('输出执行结果')
})  );
## 第四天
# 简述什么是发布订阅模式，如何实现发布订阅模式
发布-订阅也叫观察者模式，在javascript中常应用为事件监听。
1.确定充当发布者的对象 
2.然后给发布者添加一个缓存列表，里面放入回调函数 
3.当事件发生时，遍历并触发订阅这个事件的缓存列表
# 继承
class Animal {
			constructor(name) {
			    this.name = name;
			}
			getName(){
				return this.name;
			}
		}
		
		class Dog extends Animal {
			constructor(name) {
			    super(name)
			}
		}
		
		var dog = new Dog('freddy');
		console.log(dog.getName() + 'says';
# 解析 URL 参数为对象
function parseParam(url) {
  // 先将字符串通过 split 方法，以 "?" 为分割符将其分割成数组；
  // 该数组有两个元素，第一个为空字符串，第二个为 url 参数字符串
  let arr = url.split('?')
  // 将参数字符串以 "&" 符号为分隔符进行分割
  let params = arr[1].split('&')
  // 定义一个数组用于存储参数
  let obj = {}  
  // 通过循环将参数以键值对的形式存储在变量 obj 中
  for (let i = 0; i < params.length; i++) {
    let arr_params = params[i].split('=')
    obj[arr_params[0]] = arr_params[1]
  }

  return obj
}
parseParam("http://www.baidu.com?name=yihang&password=123456")


## 第五天
# 简述react生命周期以及每个生命周期的作用，使用自己的话描述出来。
componentWillMount
在渲染前调用,在客户端也在服务端。
componentDidMount
在第一次渲染后调用，只在客户端。之后组件已经生成了对应的DOM结构，可以通过this.getDOMNode()来进行访问。 如果你想和其他JavaScript框架一起使用，可以在这个方法中调用setTimeout, setInterval或者发送AJAX请求等操作(防止异步操作阻塞UI)。
componentWillReceiveProps
在组件接收到一个新的 prop (更新后)时被调用。这个方法在初始化render时不会被调用。
shouldComponentUpdate
返回一个布尔值。在组件接收到新的props或者state时被调用。在初始化时或者使用forceUpdate时不被调用。
可以在你确认不需要更新组件时使用。
componentWillUpdate
在组件接收到新的props或者state但还没有render时被调用。在初始化时不会被调用。
componentDidUpdate
在组件完成更新后立即调用。在初始化时不会被调用。
componentWillUnmount
在组件从 DOM 中移除的时候立刻被调用。

# 函数组件和class的组件的区别
只管直观区别，函数组件的代码量较少，相比类组件更加简洁。
函数组件看似只是一个返回React元素的函数，其实体现的是无状态组件（Stateless Components）的思想。函数组件中没有this，没有state，也没有生命周期，这就决定了函数组件都是展示性组件，接收props，渲染DOM，而不关注其他逻辑。
因为函数组件不需要考虑组件状态和组件生命周期方法中的各种比较校验，所以有很大的性能提升空间。
this.setState 回调函数什么时候执行
This.setState()的回调函数在state更新完毕后执行

# 常用的hooks有哪些，什么作用
useState: 返回state，更新state（函数式更新）
const [state, setState] = useState(initialState);
useEffect: 默认情况下，useEffect将在每轮的渲染结束之后运行。
useReducer: 接收一个形如 (state, action) => newState 的 reducer，并返回当前的 state 以及与其配套的 dispatch 方法。
当state逻辑比较复杂且包含多个子值，或者下一个state依赖于之前的state时，可以用上。
# react和vue的区别
数据是不是可变的
通过js操作一切还是各自的处理方式
类式的组件写法还是声明式的写法
什么功能内置，什么交给社区去做

